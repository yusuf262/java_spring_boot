package id.springlatihan.library.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.springlatihan.library.payloads.request.BookRequest;
import id.springlatihan.library.payloads.response.ResponseData;
import id.springlatihan.library.services.book.BookService;

@RestController
@RequestMapping("/books")
public class BookController {
    @Autowired
    private BookService bookService;

    @PostMapping
    public ResponseEntity<Object> createdBook(@RequestBody BookRequest bookRequest) {
        try {
            ResponseData responseData = bookService.createBookService(bookRequest);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.internalServerError().body(e.getMessage());

        }
    }
    @GetMapping
    public ResponseEntity<Object> getBooks(@RequestParam(value = "status", defaultValue = "") Boolean status) {
        try {
            ResponseData responseData = bookService.getBooksService(status);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.internalServerError().body(e.getMessage());

        }      
    }

    @GetMapping("/{idBook}")
    public ResponseEntity<Object> getBookById(@PathVariable Long idBook){
        try {
            ResponseData responseData = bookService.getBookByIdService(idBook);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }

    @PutMapping("/{idBook}")
    public ResponseEntity<Object> updateBookById(@PathVariable Long idBook, @RequestBody BookRequest bookRequest) {
        //TODO: process POST request
        try {
            ResponseData responseData = bookService.updateBookByIdService(idBook,bookRequest);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }
    @DeleteMapping("/{idBook}") //soft deleted
    public ResponseEntity<Object> deleteBookById(@PathVariable Long idBook) {
        //TODO: process POST request
        try {
            ResponseData responseData = bookService.deleteBookService(idBook);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }
    
}
