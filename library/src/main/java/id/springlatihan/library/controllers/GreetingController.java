package id.springlatihan.library.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello") //untuk mapping route yang sama
public class GreetingController {
    //RequestParam
    @GetMapping //("/hello") anotasi class
    //@RequestParam anotasi atribut
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
      return String.format("Hello %s!", name);
    }

    //PathVariable
    @PostMapping("/post/{sapa}/{number}")
    public String helloPost(@PathVariable("sapa") String sapa, @PathVariable("number") Integer number){
        return "ini method dari hello post mapping "+sapa+" "+number;
    }

    @PutMapping("/put")
    public String helloPut(@RequestBody String kalimat){
        return "ini hello dari put mapping, kalimatnya "+kalimat;
    }
}