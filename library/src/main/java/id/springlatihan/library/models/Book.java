package id.springlatihan.library.models;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //semua atribute private otomatis punya/jadi settet/getter
@NoArgsConstructor  // tidak perlu buat constructor kosongan
@Entity // semua atribut jadi entitas
@Table(name = "books")
public class Book {
    @Id //primary id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //auot inc
    private long id;

    @Column(length = 100)
    private String title;

    private String publisher, author, year, category;

    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime createdAt;

    @UpdateTimestamp
    @JsonIgnore
    private LocalDateTime updatedAt;

    
    private Boolean isDeleted = false;

    //contructor custom
    public Book(String title, String publisher, String author, String year, String category) {
        this.title = title;
        this.publisher = publisher;
        this.author = author;
        this.year = year;
        this.category = category;
    }   
    
}
