package id.springlatihan.library.services.book;

import id.springlatihan.library.payloads.request.BookRequest;
import id.springlatihan.library.payloads.response.ResponseData;

public interface BookService {
    // kerangka method crud
    // Create book
    ResponseData createBookService(BookRequest bookRequest);

    //Read All Books
    ResponseData getBooksService(Boolean status);

    //Read Book by Id
    ResponseData getBookByIdService(Long idBook);

    //Update
    ResponseData updateBookByIdService(Long idBook, BookRequest bookRequest);

    //Delete
    ResponseData deleteBookService(Long idBook);
}
