package id.springlatihan.library.services.book;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import id.springlatihan.library.models.Book;
import id.springlatihan.library.payloads.request.BookRequest;
import id.springlatihan.library.payloads.response.ResponseData;
import id.springlatihan.library.repositories.BookRepository;

@Service
public class BookServiceImpl implements BookService {
    // Instance object
    @Autowired
    BookRepository bookRepository;

    // @Autowired
    // private Book book = new Book();

    @Override
    public ResponseData createBookService(BookRequest bookRequest) {
        // TODO Auto-generated method stub
        // if (bookRequest.getJudul()!= null && bookRequest.getKategori()!=null && bookRequest.getPenerbit()!=null && bookRequest.getTahun()!=null && bookRequest.getPengarang()!=null) {
            
        // } else {
            
        // }
        Book book = new Book();
        if(bookRequest.getJudul()!=null){
            book.setTitle(bookRequest.getJudul());
        }
        if(bookRequest.getKategori()!=null){
            book.setCategory(bookRequest.getKategori());
        }
        if(bookRequest.getPenerbit()!=null){
            book.setPublisher(bookRequest.getPenerbit());
        }
        if(bookRequest.getTahun()!=null){
            book.setYear(bookRequest.getTahun());
        }        
        if(bookRequest.getPengarang()!=null){
            book.setAuthor(bookRequest.getPengarang());
        }
        //save to dB
        bookRepository.save(book);

        //Object response data
        ResponseData responseData = new ResponseData(HttpStatus.CREATED.value(), "Success", book);
        return responseData;
    }

    @Override
    public ResponseData getBooksService(Boolean status) {
        // TODO Auto-generated method stub
        List<Book> books;
        if (status == null) {
            books = bookRepository.findAll();
        } else {
            books = bookRepository.findByIsDeleted(status);
        }        
        ResponseData responseData = new ResponseData(HttpStatus.OK.value(), "success", books);
        return responseData;
        //return new ResponseData(HttpStatus.OK.value(), "success", books);
    }

    @Override
    public ResponseData getBookByIdService(Long idBook) {
        // TODO Auto-generated method stub
        //Find Book
        Optional<Book> bookFind = bookRepository.findById(idBook);
        ResponseData responseData;        
        if (bookFind.isPresent()) {
            responseData = new ResponseData(HttpStatus.OK.value(), "success", bookFind.get());
        } else {
            responseData = new ResponseData(HttpStatus.NOT_FOUND.value(), "Not Found", null);            
        }
        return responseData;
    }

    @Override
    public ResponseData updateBookByIdService(Long idBook, BookRequest bookRequest) {
        // TODO Auto-generated method stub
        Optional<Book> bookFind = bookRepository.findById(idBook);
        ResponseData responseData;
        if (bookFind.isPresent()) {
            Book book = bookFind.get();
            if(bookRequest.getJudul()!=null){
                book.setTitle(bookRequest.getJudul());
            }
            if(bookRequest.getKategori()!=null){
                book.setCategory(bookRequest.getKategori());
            }
            if(bookRequest.getPenerbit()!=null){
                book.setPublisher(bookRequest.getPenerbit());
            }
            if(bookRequest.getTahun()!=null){
                book.setYear(bookRequest.getTahun());
            }        
            if(bookRequest.getPengarang()!=null){
                book.setAuthor(bookRequest.getPengarang());
            }
            //save db
            bookRepository.save(book);
            responseData = new ResponseData(HttpStatus.OK.value(), "success", book);
        } else {
            responseData = new ResponseData(HttpStatus.NOT_FOUND.value(), "Not Found", null);
        }
        return responseData;
    }

    @Override
    public ResponseData deleteBookService(Long idBook) {
        // TODO Auto-generated method stub
        Optional<Book> bookFind = bookRepository.findById(idBook);
        ResponseData responseData;
        if (bookFind.isPresent()) {  
            //bookRepository.deleteById(idBook);
            Book book = bookFind.get();
            book.setIsDeleted(true);

            //save
            bookRepository.save(book);                 
            responseData = new ResponseData(HttpStatus.OK.value(), "success", null);
        } else {
            responseData = new ResponseData(HttpStatus.NOT_FOUND.value(), "Not Found", null);            
        }
        return responseData;
    }    
}
